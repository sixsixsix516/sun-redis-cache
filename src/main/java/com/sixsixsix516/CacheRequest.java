package com.sixsixsix516;

/**
 * @author SUN
 * @date 2023/5/10
 */
public interface CacheRequest {
    String request(String key);
}
