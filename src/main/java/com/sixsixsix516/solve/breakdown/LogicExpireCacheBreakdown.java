package com.sixsixsix516.solve.breakdown;

import com.sixsixsix516.CacheRequest;
import com.sixsixsix516.redis.DbUtil;
import com.sixsixsix516.redis.RedisUtil;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

/**
 * 逻辑过期
 *
 * @author SUN
 * @date 2023/5/10
 */
public class LogicExpireCacheBreakdown extends CacheBreakdown implements CacheRequest {

    @Override
    public String request(String key) {
        String cacheValue = RedisUtil.get(key);
        if (StringUtils.hasLength(cacheValue)) {

            String[] cacheValueArray = cacheValue.split(":");

            cacheValue = cacheValueArray[0];
            // 过期时间
            String expireDay = cacheValueArray[1];

            // 过期时间判断（这里只是意思一下，随便写的）
            if (expireDay.equals(LocalDate.now().toString())) {

                boolean lockResult = tryLock(key);
                if (lockResult) {
                    // 抢到锁了
                    // 过期了，异步更新缓存
                    new Thread(() -> {
                        try {
                            // 更新缓存数据
                            setCache(key);
                        } finally {
                            // 释放锁
                            unLock(key);
                        }
                    }).start();
                }
                // 没抢到锁就用旧数据直接返回
            }
            return cacheValue;
        }

        setCache(key);
        return cacheValue;
    }

    private void setCache(String key) {
        String cacheValue = DbUtil.get(key);

        //  加入 过期时间
        LocalDate expireDay = LocalDate.now().plusDays(1);
        cacheValue = cacheValue + ";" + expireDay;
        RedisUtil.set(key, cacheValue);
    }

    private boolean tryLock(String key) {
        Boolean result = RedisUtil.setNx(key, "1", 10, TimeUnit.SECONDS);
        return result != null && result;
    }

    private void unLock(String key) {
        RedisUtil.delete(key);
    }
}