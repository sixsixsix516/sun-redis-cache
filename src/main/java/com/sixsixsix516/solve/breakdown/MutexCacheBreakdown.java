package com.sixsixsix516.solve.breakdown;

import com.sixsixsix516.CacheRequest;
import com.sixsixsix516.redis.RedisUtil;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

/**
 * @author SUN
 * @date 2023/5/10
 */
public class MutexCacheBreakdown extends CacheBreakdown implements CacheRequest {

    @Override
    public String request(String key) {

        String cacheValue = RedisUtil.get(key);
        if (StringUtils.hasLength(cacheValue)) {
            return cacheValue;
        }

        boolean lockResult = tryLock(key);
        while (!lockResult) {
            // 没拿到锁，等待一会继续尝试获取锁
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            lockResult = tryLock(key);
        }

        // 拿到了锁，开始缓存数据
        try {
            cacheValue = "666";
            RedisUtil.set(key, cacheValue);
        } finally {
            // 释放锁
            unLock(key);
        }

        return cacheValue;
    }


    private boolean tryLock(String key) {
        Boolean result = RedisUtil.setNx(key, "1", 10, TimeUnit.SECONDS);
        return result != null && result;
    }

    private void unLock(String key) {
        RedisUtil.delete(key);
    }

}
