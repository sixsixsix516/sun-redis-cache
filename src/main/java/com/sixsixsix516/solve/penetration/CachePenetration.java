package com.sixsixsix516.solve.penetration;

/**
 * 缓存穿透
 *
 * 缓存穿透是指 客户端请求的数据在缓存和数据库中 都不存在，这样缓存永远不会生效，这些请求都会打到数据库。
 *
 * 解决方案
 * 1. 缓存空对象
 * 2. 布隆过滤
 *
 * 其他
 *  1. 增强 id 复杂度，避免被猜测 id 规律，就可以提前做好基础格式校验
 *  2. 加强用户权限校验
 *  3. 做好热点参数的限流
 *
 * @author SUN
 * @date 2023/5/10
 */
public class CachePenetration {


}
