package com.sixsixsix516.solve.penetration;

import com.sixsixsix516.CacheRequest;

/**
 * 布隆过滤器方式
 *
 * 请求来时，先通过布隆过滤器进行判断，如果布隆过滤器判定不存在则直接返回不存在，如果判定存在，则才去往下走查询缓存、数据库
 *
 * 优点：内存占用较少，没有多余的key
 * 缺点：实现复杂，存在误判可能
 *
 * @author SUN
 * @date 2023/5/10
 */
public class BloomFilterCachePenetration extends CachePenetration implements CacheRequest {

    @Override
    public String request(String key) {
        return null;
    }

}
