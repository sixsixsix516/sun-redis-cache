package com.sixsixsix516.solve.penetration;

import com.sixsixsix516.CacheRequest;
import com.sixsixsix516.redis.DbUtil;
import com.sixsixsix516.redis.RedisUtil;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

/**
 * 缓存穿透解决：缓存空对象
 *
 * 如果缓存 和 数据库层 都未命中就 缓存空对象返回
 *
 * @author SUN
 * @date 2023/5/10
 */
public class CacheNullValueCachePenetration extends CachePenetration implements CacheRequest {

    @Override
    public String request(String key) {
        String cacheValue = RedisUtil.get(key);
        if (StringUtils.hasLength(cacheValue)) {
            return cacheValue;
        }

        // 缓存中没有，查数据库
        cacheValue = DbUtil.get(key);
        if (StringUtils.hasLength(cacheValue)) {
            // 还没有，缓存空值
            cacheValue = "";

            // FIXME 问题 1：额外的内存消耗
            // FIXME 问题 2：可能造成短期的数据不一致
            //              解决方法：调整 ttl 值，新增数据时主动删除空对象值
            RedisUtil.set(key, cacheValue, 1, TimeUnit.MILLISECONDS);
        }

        return cacheValue;
    }

}
