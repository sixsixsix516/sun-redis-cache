package com.sixsixsix516.solve.avalanche;

/**
 * 缓存雪崩
 *
 * 缓存雪崩是指 同一时段大量的缓存 key 同时失效或者 redis 服务宕机，导致大量请求到达数据库，带来巨大压力
 *
 * 解决方案
 *
 * 1. 给不同的 key 的 ttl 添加随机值
 * 2. 利用 redis 集群提高服务的可用性
 * 3. 缓存失败时进入降级限流策略，保护数据库
 * 4. 给业务添加多级缓存（nginx、JVM）
 *
 * @author SUN
 * @date 2023/5/10
 */
public class CacheAvalanche {
}
