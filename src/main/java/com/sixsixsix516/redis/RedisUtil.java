package com.sixsixsix516.redis;

import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * @author SUN
 * @date 2023/5/10
 */
public class RedisUtil {

    private static StringRedisTemplate redisTemplate;

    public static String get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public static void set(String key, String cacheValue) {
        redisTemplate.opsForValue().set(key, cacheValue);
    }

    public static void set(String key, String cacheValue, int time, TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, cacheValue, time, timeUnit);
    }

    public static Boolean setNx(String key, String cacheValue, int time, TimeUnit timeUnit) {
        return redisTemplate.opsForValue().setIfAbsent(key, cacheValue, time, timeUnit);
    }

    public static void delete(String key) {
        redisTemplate.delete(key);
    }
}
