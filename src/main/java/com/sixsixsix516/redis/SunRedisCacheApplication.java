package com.sixsixsix516.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SunRedisCacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(SunRedisCacheApplication.class, args);
    }

}
